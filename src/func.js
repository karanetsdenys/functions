const getSum = (str1, str2) => {
  let result = 0, num1 = 0, num2 = 0;

  if(typeof(str1) != 'string' || typeof(str2) != 'string'){
    return false;
  }
  else if(isNaN(Number(str1)) || isNaN(Number(str1))){
    return false;
  }

  num1 = Number(str1);
  num2 = Number(str2);
  result = num1 + num2;

  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = '';
  let postCount = 0;
  let commentsCount = 0;

  for(let i = 0; i < listOfPosts.length; i++){
    if(listOfPosts[i]['author'] == authorName) postCount++;
    if(typeof listOfPosts[i]['comments'] != 'undefined'){
      for(let j = 0; j < listOfPosts[i]['comments'].length; j++){
        if(listOfPosts[i]['comments'][j]['author'] == authorName){
          commentsCount++;
        }
      }
    }
  }
  result = `Post:${postCount},comments:${commentsCount}`;

  return result;
};

const tickets=(people)=> {
  let total = 0;

  for(let i = 0; i < people.length; i++){
    if(Number(people[i]) == 25) total++;
  }

  return total > 2 ? 'YES' : 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
